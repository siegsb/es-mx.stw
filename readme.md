# Bienvenido al repositorio oficial de la traducci�n de espa�ol mexicano para Survive The Wild

Aqu� podr�s encontrar todas las versiones de la traducci�n a espa�ol mexicano para el audiojuego Survive The Wild.  
Si quieres colaborar basta con que crees tu cuenta en Bitbucket y empieces a darme tus forks para integrarlos al proyecto principal.

## Los colaboradores de esta traducci�n son:  
* Paris N. Baltazar Salguero
